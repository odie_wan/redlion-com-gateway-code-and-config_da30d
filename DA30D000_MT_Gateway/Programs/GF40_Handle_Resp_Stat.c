void GF40_Handle_Resp_Stat(void)
{
//---Parse status character
// Should get a 4 byte response
// Byte0: StatusChar[GF40s.GF40_Idx]
//          "N": No error or alarm
//          "Z": Executing zero pt cal
//          "A": Alarm exists
//          "E": Error exists
//          "X": ALarm(s) and error(s) exist
//
// Byte1: MFC ID MSB
// Byte2: MFC ID LSB
// Byte0: CR

Counters.mfc_hnd_rsp_stat_cnt++;

GF40s0.StatusChar[GF40s.GF40_Idx] = Mid(GF40s0.SerRxBuf[GF40s.GF40_Idx], 0, 1);
GF40s0.MFC_Ok_Str[GF40s.GF40_Idx] = Mid(GF40s0.SerRxBuf[GF40s.GF40_Idx], 0, 2);


if (GF40s0.MFC_Ok_Str[GF40s.GF40_Idx] == "OK") {
    GF40s0.MFC_ACK[GF40s.GF40_Idx] = true;
    GF40s0.ZeroPtCal[GF40s.GF40_Idx] = 0;
    GF40s0.AlarmsPresent[GF40s.GF40_Idx] = 0;
    GF40s0.ErrorsPresent[GF40s.GF40_Idx] = 0;
    GF40s0.ParseErr[GF40s.GF40_Idx] = 0;

}

//---NACK
else if(GF40s0.MFC_Ok_Str[GF40s.GF40_Idx] == "NG") {
    GF40s0.MFC_ACK[GF40s.GF40_Idx] = false;
    GF40s0.ZeroPtCal[GF40s.GF40_Idx] = 0;
    GF40s0.AlarmsPresent[GF40s.GF40_Idx] = 0;
    GF40s0.ErrorsPresent[GF40s.GF40_Idx] = 0;
    GF40s0.ParseErr[GF40s.GF40_Idx] = 0;
}
//---Nominal
else if(GF40s0.StatusChar[GF40s.GF40_Idx] == "N") {
    GF40s0.MFC_ACK[GF40s.GF40_Idx] = true;
    GF40s0.ZeroPtCal[GF40s.GF40_Idx] = 0;
    GF40s0.AlarmsPresent[GF40s.GF40_Idx] = 0;
    GF40s0.ErrorsPresent[GF40s.GF40_Idx] = 0;
    GF40s0.ParseErr[GF40s.GF40_Idx] = 0;
}
//---Zero pt cal in progress
else if(GF40s0.StatusChar[GF40s.GF40_Idx] == "Z") {
    GF40s0.MFC_ACK[GF40s.GF40_Idx] = true;
    GF40s0.ZeroPtCal[GF40s.GF40_Idx] = 1;
    GF40s0.AlarmsPresent[GF40s.GF40_Idx] = 0;
    GF40s0.ErrorsPresent[GF40s.GF40_Idx] = 0;
    GF40s0.ParseErr[GF40s.GF40_Idx] = 0;
}
//---Alarms present
else if(GF40s0.StatusChar[GF40s.GF40_Idx] == "A"){
    GF40s0.MFC_ACK[GF40s.GF40_Idx] = true;
    GF40s0.ZeroPtCal[GF40s.GF40_Idx] = 0;
    GF40s0.AlarmsPresent[GF40s.GF40_Idx] = 1;
    GF40s0.ErrorsPresent[GF40s.GF40_Idx] = 0;
    GF40s0.ParseErr[GF40s.GF40_Idx] = 0;
}
//---Errors present
else if(GF40s0.StatusChar[GF40s.GF40_Idx] == "E"){
    GF40s0.MFC_ACK[GF40s.GF40_Idx] = true;
    GF40s0.ZeroPtCal[GF40s.GF40_Idx] = 0;
    GF40s0.AlarmsPresent[GF40s.GF40_Idx] = 0;
    GF40s0.ErrorsPresent[GF40s.GF40_Idx] = 1;
    GF40s0.ParseErr[GF40s.GF40_Idx] = 0;
}
//---Alarms and errors present
else if(GF40s0.StatusChar[GF40s.GF40_Idx] == "X"){
    GF40s0.MFC_ACK[GF40s.GF40_Idx] = true;
    GF40s0.ZeroPtCal[GF40s.GF40_Idx] = 0;
    GF40s0.AlarmsPresent[GF40s.GF40_Idx] = 1;
    GF40s0.ErrorsPresent[GF40s.GF40_Idx] = 1;
    GF40s0.ParseErr[GF40s.GF40_Idx] = 0;

}

//---Only other valid response possible is a response starting w/ the MFC SN, which is purely numeric
else {
    GF40s0.MFC_ACK[GF40s.GF40_Idx] = false;
    GF40s0.ZeroPtCal[GF40s.GF40_Idx] = 0;
    GF40s0.AlarmsPresent[GF40s.GF40_Idx] = 0;
    GF40s0.ErrorsPresent[GF40s.GF40_Idx] = 0;
    GF40s0.ParseErr[GF40s.GF40_Idx] = 1;
}
}
