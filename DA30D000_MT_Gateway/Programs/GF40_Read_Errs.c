void GF40_Read_Errs(void)
{
// Reads the error status. The response data is in a hexa-decimal
// ASCII format representing bit flags. If a bit is set (that is, =
// 1), then the corresponding condition is true.
// Format = [STX] idRER [CR]
// Response data = [status] xx [CR]
// Bit 0 = Communication error
// Bit 1 = None
// Bit 2 = EEPROM error
// Bit 3 = Zero Point Correction Error 1. Zero
// point deviation is > �10% of the last
// calibrated value.
// Bit 4 = Zero Point Correction Error 2. Zero
// point deviation is > �10% of the default value
// set at shipment.
// Bits 5 through 7 = Not used

Counters.mfc_read_err_cnt++;

GF40s0.ComErr[GF40s.GF40_Idx] = GF40s0.Error_Word[GF40s.GF40_Idx] & 0x01;
GF40s0.FlashErr[GF40s.GF40_Idx] = GF40s0.Error_Word[GF40s.GF40_Idx] & 0x04;
GF40s0.ZeroPtErr00[GF40s.GF40_Idx] = GF40s0.Error_Word[GF40s.GF40_Idx] & 0x08;
GF40s0.ZeroPtErr01[GF40s.GF40_Idx] = GF40s0.Error_Word[GF40s.GF40_Idx] & 0x10;
}
