int GF40_Reply_Handler(void)
{
Diagnostics.gf_reply_handler_st = 97;
GF40_Rx();

Counters.mfc_rply_hnd_cnt++;

//---Rx Bytes ready to read
if (GF40s0.BytesReady[GF40s.GF40_Idx] == true)
{
    GF40_Handle_Resp_Stat();
    Diagnostics.gf_reply_handler_st = 98;

    //---Data parsed ok: check error state
    if (!GF40s0.ParseErr[GF40s.GF40_Idx])
    {
        GF40_Read_Errs();

        Diagnostics.gf_reply_handler_st = 0;
    }
    //---Parse error: response received, but not understood
    else
    {
        Diagnostics.gf_reply_handler_st = 1;
    }
}
//---No data to read---
else
    Diagnostics.gf_reply_handler_st = 2;


return Diagnostics.gf_reply_handler_st;

}
