void Init(void)
{
Defines.GF40_ENG_BOOT           = 0;
Defines.GF40_ENG_IDLE           = 1;
Defines.GF40_ENG_START          = 2;
Defines.GF40_ENG_OPERATIONAL    = 3;
Defines.GF40_ENG_RESET          = 4;
Defines.GF40_ENG_MAINT_IDLE     = 5;
Defines.GF40_ENG_MAINT_SET_ID   = 6;

Defines.MTS_ENG_BOOT           = 0;
Defines.MTS_ENG_IDLE           = 1;
Defines.MTS_ENG_START          = 2;
Defines.MTS_ENG_OPERATIONAL    = 3;
Defines.MTS_ENG_RESET          = 4;
Defines.MTS_ENG_MAINT          = 5;

Enums.MFC_Gasses[0] = "Unspecified";
Enums.MFC_Gasses[1] = "Air";
Enums.MFC_Gasses[2] = "O2";
Enums.MFC_Gasses[3] = "CO2";
Enums.MFC_Gasses[4] = "N2";

Enums.MFC_Exec_St_Str[0] = "Idle";
Enums.MFC_Exec_St_Str[1] = "Init Cfg Read";
Enums.MFC_Exec_St_Str[2] = "Wait for cfg read to complete";
Enums.MFC_Exec_St_Str[3] = "Init Cfg Write";
Enums.MFC_Exec_St_Str[4] = "Wait for cfg write to complete";
Enums.MFC_Exec_St_Str[5] = "Init Err Check";
Enums.MFC_Exec_St_Str[6] = "Wait for Err Check to complete";
Enums.MFC_Exec_St_Str[7] = "Init Op Query";
Enums.MFC_Exec_St_Str[8] = "Wait for Op Query to complete";




Enums.MFC_OpQuery_St[0] = "Idle";
Enums.MFC_OpQuery_St[1] = "Send SP";
Enums.MFC_OpQuery_St[2] = "Waiting for ACK";
Enums.MFC_OpQuery_St[3] = "Send Valve State";
Enums.MFC_OpQuery_St[4] = "Waiting for ACK";
Enums.MFC_OpQuery_St[5] = "Query Mdot";
Enums.MFC_OpQuery_St[6] = "Waiting for ACK";
Enums.MFC_OpQuery_St[7] = "Query Flash Error State";
Enums.MFC_OpQuery_St[8] = "Waiting for ACK";
Enums.MFC_OpQuery_St[9] = "Query ZeroPt0 Error State";
Enums.MFC_OpQuery_St[10] = "Waiting for ACK";
Enums.MFC_OpQuery_St[11] = "Query ZeroPt1 Error State";
Enums.MFC_OpQuery_St[12] = "Waiting for ACK";
Enums.MFC_OpQuery_St[13] = "Query Com Error State";
Enums.MFC_OpQuery_St[14] = "Waiting for ACK";
Enums.MFC_OpQuery_St[15] = "Done";
Enums.MFC_OpQuery_St[16] = "Error";

Enums.MFC_Config_Read_Query_St[0] = "Idle";
Enums.MFC_Config_Read_Query_St[1] = "Query ID";
Enums.MFC_Config_Read_Query_St[2] = "Process Response";
Enums.MFC_Config_Read_Query_St[3] = "Query Gas name";
Enums.MFC_Config_Read_Query_St[4] = "Process Response";
Enums.MFC_Config_Read_Query_St[5] = "Done w/o Errors";
Enums.MFC_Config_Read_Query_St[6] = "Done w/ Errors: Query errors";

Enums.MFC_Config_Write_Query_St[0] = "Idle";
Enums.MFC_Config_Write_Query_St[1] = "Send SP";
Enums.MFC_Config_Write_Query_St[2] = "Waiting for ACK";
Enums.MFC_Config_Write_Query_St[3] = "Send Valve State";
Enums.MFC_Config_Write_Query_St[4] = "Waiting for ACK";
Enums.MFC_Config_Write_Query_St[5] = "Query Mdot";
Enums.MFC_Config_Write_Query_St[6] = "Waiting for ACK";
Enums.MFC_Config_Write_Query_St[7] = "Query Flash Error State";
Enums.MFC_Config_Write_Query_St[8] = "Waiting for ACK";
Enums.MFC_Config_Write_Query_St[9] = "Query ZeroPt0 Error State";
Enums.MFC_Config_Write_Query_St[10] = "Waiting for ACK";
Enums.MFC_Config_Write_Query_St[11] = "Query ZeroPt1 Error State";
Enums.MFC_Config_Write_Query_St[12] = "Waiting for ACK";
Enums.MFC_Config_Write_Query_St[13] = "Query Com Error State";
Enums.MFC_Config_Write_Query_St[14] = "Waiting for ACK";
Enums.MFC_Config_Write_Query_St[15] = "Done";
Enums.MFC_Config_Write_Query_St[16] = "Error";

if(GatewaySettings.MT_Stale_Data_Threshold == 0)
    GatewaySettings.MT_Stale_Data_Threshold = 10;

if(GatewaySettings.MT_Stale_Cnt_Max == 0)
	GatewaySettings.MT_Stale_Cnt_Max = 255;

if(GatewaySettings.MFC_CfgW_Wait_Lim == 0)
    GatewaySettings.MFC_CfgW_Wait_Lim = 5;

if(GatewaySettings.MFC_Err_Wait_Lim == 0)
    GatewaySettings.MFC_Err_Wait_Lim = 5;

if(GatewaySettings.MFC_Op_Wait_Lim == 0)
    GatewaySettings.MFC_Op_Wait_Lim = 5;

if(GatewaySettings.MFC_CfgR_Wait_Lim == 0)
    GatewaySettings.MFC_CfgR_Wait_Lim = 5;

MT_Diag.MT_Scale_Eng_State = Defines.MTS_ENG_BOOT;

if(MT_Comms.MT_Ser_Port == 0)
	MT_Comms.MT_Ser_Port = 2;

if(GatewaySettings.NUM_MFCs == 0)
	GatewaySettings.NUM_MFCs = 2;
	
GF40s.GF40_Engine_State = Defines.GF40_ENG_BOOT;


GF40s0.MFC_En[0] = true;
GF40s0.MFC_En[1] = true;
GF40s0.MFC_En[2] = true;
GF40s0.MFC_En[3] = true;
GF40s0.MFC_En[4] = true;
GF40s0.MFC_En[5] = true;
GF40s0.MFC_En[6] = true;
GF40s0.MFC_En[7] = true;

GF40s0.MFC_En[0] = true;
GF40s0.MFC_En[1] = true;
GF40s0.MFC_En[2] = true;
GF40s0.MFC_En[3] = true;
GF40s0.MFC_En[4] = true;
GF40s0.MFC_En[5] = true;
GF40s0.MFC_En[6] = true;
}
