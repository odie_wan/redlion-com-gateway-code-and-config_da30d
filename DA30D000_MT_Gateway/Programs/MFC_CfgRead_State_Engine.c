void MFC_CfgRead_State_Engine(void)
{
    if(GF40s0.CfgReadReq[GF40s.GF40_Idx] && GF40s0.CfgReadReq[GF40s.GF40_Idx] != GF40s0.CfgReadReqShdw[GF40s.GF40_Idx])
        GF40s0.CfgReadQueryState[GF40s.GF40_Idx] = 1;

    GF40s0.CfgReadReqShdw[GF40s.GF40_Idx] = GF40s0.CfgReadReq[GF40s.GF40_Idx];

    Counters.mfc_cfgR_eng_cnt++;

    switch(GF40s0.CfgReadQueryState[GF40s.GF40_Idx]) {
        case 0: // Idle
            break;

        case 1: // Req ID given serial number
            if(GF40s0.MFC_En[GF40s.GF40_Idx] == true && GF40s0.SN[GF40s.GF40_Idx] != "") {
	        GF40s0.SerTxBuf[GF40s.GF40_Idx] = "\x0200RID" + GF40s0.SN[GF40s.GF40_Idx] + "\r";
	        PortPrint(3, GF40s0.SerTxBuf[GF40s.GF40_Idx]);
	        GF40s0.SerTxBytes[GF40s.GF40_Idx] += Len(GF40s0.SerTxBuf[GF40s.GF40_Idx]);
	    }
	    else {
	    	GF40s0.SerTxBuf[GF40s.GF40_Idx] = "";
	    	GF40s0.CfgRExecCnt[GF40s.GF40_Idx] = 6;
	    	
	    }
            GF40s0.CfgRExecCnt[GF40s.GF40_Idx]++;
            GF40s0.CfgReadQueryState[GF40s.GF40_Idx]++;
            GF40s0.CfgReadQueryCount[GF40s.GF40_Idx]++;
            break;

        case 2:// Waiting for ACK
            switch (GF40_Reply_Handler()) {
                case 0: //---Data ok, understood ok
                    //---Parse MFC ID
                    GF40s0.Id[GF40s.GF40_Idx] = Mid(GF40s0.SerRxBuf[GF40s.GF40_Idx], 1, 2);
                    GF40s0.id_int[GF40s.GF40_Idx] = TextToInt(GF40s0.Id[GF40s.GF40_Idx], 10);

                    if(GF40s0.MFC_ACK[GF40s.GF40_Idx] == true &&
                       GF40s0.id_int[GF40s.GF40_Idx] > 0 &&
                       GF40s0.id_int[GF40s.GF40_Idx] < 63)
                        {
                        GF40s0.CfgReadQueryState[GF40s.GF40_Idx]++;
                    }
                    else {
                        GF40s0.CfgReadQueryState[GF40s.GF40_Idx] = 5;
                    }


                    GF40s0.CfgRWaitCnt[GF40s.GF40_Idx] = 0;
                    break;

                //---Parse error
                case 1:
                    GF40s0.CfgReadQueryState[GF40s.GF40_Idx] = 8;
                    GF40s0.CfgRWaitCnt[GF40s.GF40_Idx] = 0;
                    break;

                //---No response
                case 2:
                    GF40s0.CfgRWaitCnt[GF40s.GF40_Idx]++;
                    if(GF40s0.CfgRWaitCnt[GF40s.GF40_Idx] > GatewaySettings.MFC_CfgR_Wait_Lim)
                        GF40s0.CfgReadQueryState[GF40s.GF40_Idx] = 5;
                    break;

                default:
                    break;
            }
            GF40s0.BytesReady[GF40s.GF40_Idx] = false;
            break;

        case 3: // Req gas name given ID
            GF40s0.SerTxBuf[GF40s.GF40_Idx] = "\x02" + GF40s0.Id[GF40s.GF40_Idx] + "RGN\r";
            PortPrint(3, GF40s0.SerTxBuf[GF40s.GF40_Idx]);
            GF40s0.SerTxBytes[GF40s.GF40_Idx] += Len(GF40s0.SerTxBuf[GF40s.GF40_Idx]);
            GF40s0.CfgReadQueryState[GF40s.GF40_Idx]++;
            break;

        case 4:// Waiting for ACK
            switch (GF40_Reply_Handler()) {
                case 0://---Data rx, understood ok
                    //---Parse response payload
                    //---Find the start and end of payload (2nd char to 2nd to last char)
                    int crPos = Find(GF40s0.SerRxBuf[GF40s.GF40_Idx], '\r', 0);
                    int strLen = Len(GF40s0.SerRxBuf[GF40s.GF40_Idx]);
                    int nameLen = crPos - 3;

                    GF40s0.Name[GF40s.GF40_Idx] = Mid(GF40s0.SerRxBuf[GF40s.GF40_Idx], 1, nameLen);
                    GF40s0.CfgReadQueryState[GF40s.GF40_Idx] = 7;
                    GF40s0.CfgRWaitCnt[GF40s.GF40_Idx] = 0;
                    Diagnostics.gf_cfgr_st = 11;
                    break;

                case 1://---Data rx, not understood
                    GF40s0.CfgReadQueryState[GF40s.GF40_Idx] = 8;
                    GF40s0.CfgRWaitCnt[GF40s.GF40_Idx] = 0;
                    Diagnostics.gf_cfgr_st = 12;
                    break;

                case 2://---No reply
                    GF40s0.CfgRWaitCnt[GF40s.GF40_Idx]++;
                    if(GF40s0.CfgRWaitCnt[GF40s.GF40_Idx] > GatewaySettings.MFC_CfgR_Wait_Lim)
                        GF40s0.CfgReadQueryState[GF40s.GF40_Idx] = 5;
                    break;

                default:
                    break;
            }
            break;

        case 5: // No reply
            GF40s0.mfcDataOk[GF40s.GF40_Idx] = false;
            GF40s0.NoReply[GF40s.GF40_Idx] = true;
            GF40s0.MFC_State[GF40s.GF40_Idx] = 4;
            break;

        case 6: // Not configured or enabled
            GF40s0.mfcDataOk[GF40s.GF40_Idx] = false;
            GF40s0.NoReply[GF40s.GF40_Idx] = false;
            GF40s0.MFC_State[GF40s.GF40_Idx] = 1;
            break;


        case 7: // Done w/o Errors
            GF40s0.mfcDataOk[GF40s.GF40_Idx] = true;
            GF40s0.NoReply[GF40s.GF40_Idx] = false;
            GF40s0.MFC_State[GF40s.GF40_Idx] = 7;
            break;

        case 8: // Done w/ Errors: Query Error status
            GF40s0.mfcDataOk[GF40s.GF40_Idx] = false;
            GF40s0.NoReply[GF40s.GF40_Idx] = false;
            GF40s0.MFC_State[GF40s.GF40_Idx] = 10;
            break;

        default:
            break;
    }
    GF40s0.BytesReady[GF40s.GF40_Idx] = false;
}
