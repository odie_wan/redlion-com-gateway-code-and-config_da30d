void MFC_Err_State_Engine(void)
{
if(GF40s0.ErrReadReq[GF40s.GF40_Idx] && GF40s0.ErrReadReq[GF40s.GF40_Idx] != GF40s0.ErrReadReqShdw[GF40s.GF40_Idx])
    GF40s0.ErrQueryState[GF40s.GF40_Idx] = 1;


GF40s0.ErrReadReqShdw[GF40s.GF40_Idx] = GF40s0.ErrReadReq[GF40s.GF40_Idx];

Counters.mfc_err_eng_cnt++;
GF40s0.ErrExecCnt[GF40s.GF40_Idx]++;

switch(GF40s0.ErrQueryState[GF40s.GF40_Idx]) {


    case 0: // Idle
        //--- nothing
        break;


    case 1: //
        GF40s0.SerTxBuf[GF40s.GF40_Idx] = "\x02" + GF40s0.Id[GF40s.GF40_Idx] + "RER\r";
        PortPrint(3, GF40s0.SerTxBuf[GF40s.GF40_Idx]);
        GF40s0.SerTxBytes[GF40s.GF40_Idx] += Len(GF40s0.SerTxBuf[GF40s.GF40_Idx]);
        GF40s0.ErrQueryState[GF40s.GF40_Idx]++;
        GF40s0.ErrQueryCount[GF40s.GF40_Idx]++;

        break;

    case 2:// Waiting for ACK

        switch (GF40_Reply_Handler()) {
            case 0: //---Data rx, data ok
                GF40s0.ErrQueryState[GF40s.GF40_Idx] = 4;
                GF40s0.Error_Word[GF40s.GF40_Idx] = TextToInt(Mid(GF40s0.SerRxBuf[GF40s.GF40_Idx], 1, 2), 10);
                GF40s0.ErrWaitCnt[GF40s.GF40_Idx] = 0;
                GF40s0.ErrRespCount[GF40s.GF40_Idx]++;
                break;

            case 1://---Data rx, not understood
                GF40s0.ErrQueryState[GF40s.GF40_Idx] = 5;
                GF40s0.ErrWaitCnt[GF40s.GF40_Idx] = 0;
                GF40s0.ErrRespCount[GF40s.GF40_Idx]++;
                break;

            case 2://---No reply
                GF40s0.ErrWaitCnt[GF40s.GF40_Idx]++;
                if(GF40s0.ErrWaitCnt[GF40s.GF40_Idx] > GatewaySettings.MFC_Err_Wait_Lim)
                    GF40s0.ErrQueryState[GF40s.GF40_Idx] = 3;

                break;

            default:
                GF40s0.ErrQueryState[GF40s.GF40_Idx] = 5;
                break;

        }
        break;

    case 3: // No reply
        GF40s0.mfcDataOk[GF40s.GF40_Idx] = false;
        GF40s0.NoReply[GF40s.GF40_Idx] = true;
        break;

    case 4: // Done w/o Errors
        GF40s0.ParseErr[GF40s.GF40_Idx] = false;
        GF40s0.mfcDataOk[GF40s.GF40_Idx] = true;
        GF40s0.NoReply[GF40s.GF40_Idx] = false;
        break;

    case 5: // Done w/ Errors:  Error status
        GF40s0.mfcDataOk[GF40s.GF40_Idx] = false;
        GF40s0.NoReply[GF40s.GF40_Idx] = false;
        break;


    default:
        break;
}

}
