void MFC_Exec_State_Engine(void)
{

Counters.mfc_exe_eng_cnt++;


switch(GF40s.GF40_ExecState) {
    //=============================================================================================
    case 0: // Init Config Read Req

        if(GF40s0.SN[GF40s.GF40_Idx] == "0" || GF40s0.MFC_En[GF40s.GF40_Idx] == false) {
            GF40s0.SerTxBuf[GF40s.GF40_Idx] = "";
            GF40s.GF40_ExecState = 7;
        }
        //---SN is non-zero
        else {
            // ID is 0: need to read mfc cofig
            if(GF40s0.Id[GF40s.GF40_Idx] == "0"){
                GF40s0.CfgReadReq[GF40s.GF40_Idx] = true;
                GF40s.GF40_ExecState++;
            }
            else
                GF40s.GF40_ExecState = 3;
        }

        GF40s.GF40_ExecLoopCnt++;
        break;

    case 1:// Wait for cfg read to complete

        switch (GF40s0.CfgReadQueryState[GF40s.GF40_Idx]) {
            case 2:
                /* still waiting for a reply or timeout: stay here */
                break;

            case 5:
                //---No reply; done w/ this mfc till next time
                GF40s.GF40_ExecState = 7;
                GF40s0.CfgReadReq[GF40s.GF40_Idx] = false;
                break;
 	
 	    
            case 6: //--Not Configured or enabled            
            case 7: //---Done w/o errors
            case 8: //---Done w/ errors
            default:
                //---We got a reply: continue
                GF40s.GF40_ExecState++;
                GF40s0.CfgReadReq[GF40s.GF40_Idx] = false;
                break;
        }
        break;

    //=============================================================================================
    case 3: // Init Err Check
        GF40s0.ErrReadReq[GF40s.GF40_Idx] = true;
        GF40s.GF40_ExecState++;
        break;

    case 4:// Wait for Err Check to complete

        switch (GF40s0.ErrQueryState[GF40s.GF40_Idx]) {
            case 2:
                /* still waiting for a reply or timeout: stay here */
                break;

            case 3:
                //---No reply; done w/ this mfc till next time
                GF40s.GF40_ExecState = 7;
                GF40s0.ErrReadReq[GF40s.GF40_Idx] = false;
                break;


            case 4:
            case 5:
            default:
                //---We got a reply: continue
                GF40s.GF40_ExecState++;
                GF40s0.ErrReadReq[GF40s.GF40_Idx] = false;
                break;
        }

        break;

    //=============================================================================================
    case 5:// Init Op Query
        GF40s0.OpReadReq[GF40s.GF40_Idx] = true;

        GF40s.GF40_ExecState++;
        break;

    case 6:
        switch (GF40s0.OpQueryState[GF40s.GF40_Idx]) {
            case 1: //---Send valve mode: Open, closed, CL
            case 2: //---Wait for ACK/NACK
            case 3: //---Send SP if in CL mode
            case 4: //---Wait for ACK/NACK
            case 5: //---Req valve position
            case 6: //---Wait for ACK/NACK
            case 7: //---Read mass flow
            case 8: //---Wait for ACK/NACK
                /* still waiting for a reply or timeout: stay here */
                break;

            case 9://---No reply
            case 10://---We got a reply: continue
            case 11://---We got a reply but it's whacked
            default:

                GF40s.GF40_ExecState++;
                GF40s0.OpReadReq[GF40s.GF40_Idx] = false;
                break;
        }
        break;

    //=============================================================================================
    case 7: //---Placeholder state for the end of the loop

        GF40s.GF40_Idx++;

        if(GF40s.GF40_Idx >= GatewaySettings.NUM_MFCs)
            GF40s.GF40_Idx = 0;

        GF40s.GF40_ExecState = 0;
        break;

    default:
        GF40s.GF40_ExecState = 0;
        break;
}


}
