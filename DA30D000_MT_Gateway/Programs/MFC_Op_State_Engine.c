void MFC_Op_State_Engine(void)
{
if(GF40s0.OpReadReq[GF40s.GF40_Idx] && GF40s0.OpReadReq[GF40s.GF40_Idx] != GF40s0.OpReadReqShdw[GF40s.GF40_Idx]){
    // GF40s0.mfcDataOk[GF40s.GF40_Idx] = false;
    GF40s0.OpQueryState[GF40s.GF40_Idx] = 1;
}
// else if(!GF40s0.OpReadReq[GF40s.GF40_Idx]){
//     GF40s0.OpQueryState[GF40s.GF40_Idx] = 0;
// }

GF40s0.OpReadReqShdw[GF40s.GF40_Idx] = GF40s0.OpReadReq[GF40s.GF40_Idx];

GF40s0.OpExecCnt[GF40s.GF40_Idx]++;
Counters.mfc_op_eng_cnt++;


switch(GF40s0.OpQueryState[GF40s.GF40_Idx]) {


    case 0: // Idle
        //--- nothing
        break;


    //---There are three modes for the valve position:
    //   Open: fully open, ergo full flow: Open loop
    //   Closed: fully fully closed, ergo zero flow: Open loop
    //   Variable: Controlled by flow SP which is the CV for the closed loop controller for
    //      the valve: Closed loop at the system level.

    // Write valve pos.
    //    ValveStates available:
    //      0: closed: send SVC cmd
    //      1: open: send SVO cmd
    //      2: no command: for closed loop mdot controls:
    //          SVO/SVC cmds not sent
    //          SVN&SDM  cmd sent


    //    ValveSP = 0: Valve commanded closed: SVO
    //    ValveSP = 1: Valve commanded open: SVC
    //    ValveSP = 2: Enable valve control mode: SVN valve position is controlled by MassflowSP via SDC

    case 1:
        if(GF40s0.ValveCmdModeLocal[GF40s.GF40_Idx] == 0) {
            GF40s0.SerTxBuf[GF40s.GF40_Idx] = "\x02" + GF40s0.Id[GF40s.GF40_Idx] + "SVC\r";
        }
        else if(GF40s0.ValveCmdModeLocal[GF40s.GF40_Idx] == 1) {
            GF40s0.SerTxBuf[GF40s.GF40_Idx] = "\x02" + GF40s0.Id[GF40s.GF40_Idx] + "SVO\r";
        }
        else if(GF40s0.ValveCmdModeLocal[GF40s.GF40_Idx] == 2) {
            GF40s0.SerTxBuf[GF40s.GF40_Idx] = "\x02" + GF40s0.Id[GF40s.GF40_Idx] + "SVN\r";
        }

        PortPrint(3, GF40s0.SerTxBuf[GF40s.GF40_Idx]);
        GF40s0.SerTxBytes[GF40s.GF40_Idx] += Len(GF40s0.SerTxBuf[GF40s.GF40_Idx]);
        GF40s0.OpQueryState[GF40s.GF40_Idx]++;
        GF40s0.OpQueryCount[GF40s.GF40_Idx]++;
        break;

    case 2: // Wait for ACK/NACK
        switch (GF40_Reply_Handler()) {
            case 0://Data ok, parse ok
                GF40s0.CmdResp[GF40s.GF40_Idx] = Mid(GF40s0.SerRxBuf[GF40s.GF40_Idx], 1, 2);
                GF40s0.OpQueryState[GF40s.GF40_Idx]++;
                GF40s0.OpWaitCnt[GF40s.GF40_Idx] = 0;
                GF40s0.OpRespCnt[GF40s.GF40_Idx]++;;
                break;

            case 1://Data rx, but not understood
                GF40s0.OpQueryState[GF40s.GF40_Idx]++;
                GF40s0.OpWaitCnt[GF40s.GF40_Idx] = 0;
                GF40s0.OpRespCnt[GF40s.GF40_Idx]++;
                break;


            case 2://---No reply
                GF40s0.OpWaitCnt[GF40s.GF40_Idx]++;
                //---After 10 cycles w/ no response, bug out
                if(GF40s0.OpWaitCnt[GF40s.GF40_Idx] > GatewaySettings.MFC_Op_Wait_Lim)
                    GF40s0.OpQueryState[GF40s.GF40_Idx] = 9;
                break;

            default:
                break;
        }
        break;

    case 3: // Write mDot SP when in CL mode
        //---Valve control mode: send mass flow SP
        if(GF40s0.ValveCmdModeLocal[GF40s.GF40_Idx] == 2) {
            GF40s0.SerTxBuf[GF40s.GF40_Idx] = "\x02" + GF40s0.Id[GF40s.GF40_Idx] + "SDC" + AsText(GF40s0.mDotSP[GF40s.GF40_Idx]) + "\r";
            PortPrint(3, GF40s0.SerTxBuf[GF40s.GF40_Idx]);
            GF40s0.SerTxBytes[GF40s.GF40_Idx] += Len(GF40s0.SerTxBuf[GF40s.GF40_Idx]);
            GF40s0.OpQueryState[GF40s.GF40_Idx]++;
        }
        //---Not valve control mode: move on
        else {
            GF40s0.OpQueryState[GF40s.GF40_Idx] += 2;
        }
        break;


    case 4: //---Wait for ack from mfc---
        switch (GF40_Reply_Handler()) {
            case 0://Data ok, parse ok
               GF40s0.OpQueryState[GF40s.GF40_Idx]++;
                GF40s0.OpWaitCnt[GF40s.GF40_Idx] = 0;
                GF40s0.OpRespCnt[GF40s.GF40_Idx]++;
                break;

            case 1://Data rx, but not understood
                GF40s0.OpQueryState[GF40s.GF40_Idx]++;
                GF40s0.OpWaitCnt[GF40s.GF40_Idx] = 0;
                GF40s0.OpRespCnt[GF40s.GF40_Idx]++;
                break;

            case 2://---No reply
                GF40s0.OpWaitCnt[GF40s.GF40_Idx]++;
                //---After 10 cycles w/ no response, bug out
                if(GF40s0.OpWaitCnt[GF40s.GF40_Idx] > GatewaySettings.MFC_Op_Wait_Lim)
                    GF40s0.OpQueryState[GF40s.GF40_Idx] = 9;
                break;

            default:
                break;
        }

        break;

    case 5: // Read Valve position PV
        GF40s0.SerTxBuf[GF40s.GF40_Idx] = "\x02" + GF40s0.Id[GF40s.GF40_Idx] + "RVD\r";
        PortPrint(3, GF40s0.SerTxBuf[GF40s.GF40_Idx]);
        GF40s0.SerTxBytes[GF40s.GF40_Idx] += Len(GF40s0.SerTxBuf[GF40s.GF40_Idx]);

        GF40s0.OpQueryState[GF40s.GF40_Idx]++;
        break;

    case 6:// Waiting for ACK
        switch (GF40_Reply_Handler()) {
            case 0://Data ok, parse ok
                //---Parse MFC ID
                GF40s0.ValvePV[GF40s.GF40_Idx] = TextToFloat(Mid(GF40s0.SerRxBuf[GF40s.GF40_Idx], 1, 5));
                GF40s0.OpQueryState[GF40s.GF40_Idx]++;
                
                GF40s0.OpWaitCnt[GF40s.GF40_Idx] = 0;
                GF40s0.OpRespCnt[GF40s.GF40_Idx]++;
                GF40s0.ValvePVCnt[GF40s.GF40_Idx]++;
		GF40s0.now[GF40s.GF40_Idx] = GetNowTime();
		GF40s0.dTime[GF40s.GF40_Idx] = GF40s0.now[GF40s.GF40_Idx] - GF40s0.nowShadow[GF40s.GF40_Idx];
                GF40s0.nowShadow[GF40s.GF40_Idx] = GF40s0.now[GF40s.GF40_Idx];

                break;

            case 1://Data rx, but not understood
                GF40s0.OpQueryState[GF40s.GF40_Idx]++;
                GF40s0.OpWaitCnt[GF40s.GF40_Idx] = 0;
                GF40s0.OpRespCnt[GF40s.GF40_Idx]++;
                break;

            case 2://---No reply
                GF40s0.OpWaitCnt[GF40s.GF40_Idx]++;
                //---After 10 cycles w/ no response, bug out
                if(GF40s0.OpWaitCnt[GF40s.GF40_Idx] > GatewaySettings.MFC_Op_Wait_Lim)
                    GF40s0.OpQueryState[GF40s.GF40_Idx] = 9;
                break;

            default:
                break;
        }
        break;

    case 7: // Req Flow rate PV
        GF40s0.SerTxBuf[GF40s.GF40_Idx] = "\x02" + GF40s0.Id[GF40s.GF40_Idx] + "RFX\r";
        PortPrint(3, GF40s0.SerTxBuf[GF40s.GF40_Idx]);
        GF40s0.SerTxBytes[GF40s.GF40_Idx] += Len(GF40s0.SerTxBuf[GF40s.GF40_Idx]);
        GF40s0.OpQueryState[GF40s.GF40_Idx]++;
        break;

    case 8:// Waiting for ACK
        switch (GF40_Reply_Handler()) {
            case 0://Data ok, parse ok
                //---Parse response payload

                GF40s0.mDotPV[GF40s.GF40_Idx] = TextToFloat(Mid(GF40s0.SerRxBuf[GF40s.GF40_Idx], 1, 5));
                GF40s0.OpQueryState[GF40s.GF40_Idx] = 10;
                GF40s0.OpWaitCnt[GF40s.GF40_Idx] = 0;
                GF40s0.OpRespCnt[GF40s.GF40_Idx]++;
                GF40s0.mDotPVCnt[GF40s.GF40_Idx]++;
                break;

            case 1://Data rx, but not understood
                GF40s0.OpQueryState[GF40s.GF40_Idx] = 11;
                GF40s0.OpWaitCnt[GF40s.GF40_Idx] = 0;
                GF40s0.OpRespCnt[GF40s.GF40_Idx]++;
                break;

            case 2://---No reply
                GF40s0.OpWaitCnt[GF40s.GF40_Idx]++;
                //---After 10 cycles w/ no response, bug out
                if(GF40s0.OpWaitCnt[GF40s.GF40_Idx] > GatewaySettings.MFC_Op_Wait_Lim)
                    GF40s0.OpQueryState[GF40s.GF40_Idx] = 9;
                break;

            default:
                break;
        }

        break;

    case 9: // No reply
        GF40s0.mfcDataOk[GF40s.GF40_Idx] = false;
        GF40s0.NoReply[GF40s.GF40_Idx] = true;
        GF40s0.MFC_State[GF40s.GF40_Idx] = 4;
        break;

    case 10: // Done w/o Errors
        GF40s0.mfcDataOk[GF40s.GF40_Idx] = true;
        GF40s0.NoReply[GF40s.GF40_Idx] = false;
        GF40s0.MFC_State[GF40s.GF40_Idx] = 7;
        break;

    case 11: // Done w/ Errors: Query Error status
        GF40s0.mfcDataOk[GF40s.GF40_Idx] = false;
        GF40s0.NoReply[GF40s.GF40_Idx] = false;
        GF40s0.MFC_State[GF40s.GF40_Idx] = 10;
        break;


    default:
        break;
}



}
