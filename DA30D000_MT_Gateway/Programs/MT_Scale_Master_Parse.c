void MT_Scale_Master_Parse(void)
{
int pos;
int pos2;
int pos3;
int pos4;
int pos5;
int pos6;
int i;
cstring temp;

//----Typical Mettler-Toledo scale serial weight reading output is:
//    "S S      0.085 oz<cr><lf>"
//    The 2nd 'S' is the stability indication.
//      S -> measurement is stable
//      D -> measurement is not stable (D is for dynamic)
//      there are 6 spaces between the stability char and the measurement
//      and one space between the measurement (0.085) and the units (oz)
//      the string is terminated by thet <cr><lf>
//      <cr> -> carriage return '\r'
//      <lf> -> line feed '\n'

//----Typical commands:
//    @ -> reset. Resets the scale
//    Typical response is:
//    I4 A "text" where "text" will contain the scale serial #(114350697

//    I1 A "01" "2.30" "2.20" "2.30" "2.20"

MT_Counters._parse_count++;


MT_Comms.MT_Ser_Rx = PortInput(MT_Comms.MT_Ser_Port, 0, 13, 100, 45);

Push_MT_Rx_Buff(MT_Comms.MT_Ser_Rx);

//MT_Comms.rx_raw = PortInput(MT_Comms.MT_Ser_Port, 0, '\r', 100, 100);
//MT_Comms.rx_chars = PortRead(MT_Comms.MT_Ser_Port, 100);								     

MT_Ops.MT_Base_Weight_Input_Len = Len(MT_Comms.MT_Ser_Rx);
MT_Comms.MT_Ser_Rx_Bytes += MT_Ops.MT_Base_Weight_Input_Len;

if (MT_Comms.MT_Ser_Rx == "")
{
    MT_Comms.MT_Has_Serial_Data = 0;
    MT_Ops.MT_OK_Flag = 0;
    MT_Comms.MT_Ser_Rx = "<no data>";
    MT_Ops.MT_Data_Stable = 0;
    Diagnostics.step = -1;
    MT_Counters._stale_count++;
}
else
{
    MT_Comms.MT_Has_Serial_Data = 1;
    MT_Ops.MT_OK_Flag = 0;
    MT_Diag.MT_Last_Rx = MT_Comms.MT_Ser_Rx;
    MT_Diag._input_1st_char = Mid(MT_Comms.MT_Ser_Rx, 0, 1);
    MT_Diag._input_2nd_char = Mid(MT_Comms.MT_Ser_Rx, 1, 1);

    MT_Diag._input_1_2 = Mid(MT_Comms.MT_Ser_Rx, 1, 2);
    MT_Diag._input_1_3 = Mid(MT_Comms.MT_Ser_Rx, 1, 3);
    MT_Diag._input_1_4 = Mid(MT_Comms.MT_Ser_Rx, 1, 4);
    MT_Diag._input_1_5 = Mid(MT_Comms.MT_Ser_Rx, 1, 5);
    MT_Diag._input_1_5 = Mid(MT_Comms.MT_Ser_Rx, 1, 6);

    MT_Diag._input_0_2 = Mid(MT_Comms.MT_Ser_Rx, 0, 2);
    MT_Diag._input_0_3 = Mid(MT_Comms.MT_Ser_Rx, 0, 3);
    MT_Diag._input_0_4 = Mid(MT_Comms.MT_Ser_Rx, 0, 4);
    MT_Diag._input_0_5 = Mid(MT_Comms.MT_Ser_Rx, 0, 5);
    MT_Diag._input_0_5 = Mid(MT_Comms.MT_Ser_Rx, 0, 6);

    Diagnostics.step = 1;

    if (MT_Diag._input_2nd_char == "I")
    {
        Diagnostics.step = 100;
        MT_Diag.m_step = 1;
        //---List of implemented MT-SICS commands
        if (MT_Diag._input_1_3 == "I0 ")
        {
            MT_Ops.MT_OK_Flag = 1;
            Diagnostics.step = 101;
            MT_Diag.m_step = 2;
        }
        //---MT-SICS level and version: Format: [I1 A <"Level"> <"V0"> <"V1"> <"V2"> <"V3"\r\n>
        //---Ex: [I1 A "0123" "2.00" "2.20" "1.00" "1.50"\r\n]
        else if (MT_Diag._input_1_4 == "I1 A")
        {
            pos:= Find(MT_Comms.MT_Ser_Rx, '"', 0);
            pos2:= Find(MT_Comms.MT_Ser_Rx, '"', 1);
            // MT_Meta_MT_SICS_Level := MT_Raw_Serial;
            MT_Meta.MT_Meta_MTSICS_Level:= Mid(MT_Comms.MT_Ser_Rx, pos + 1, pos2 - pos - 1);
            MT_Diag.m_step = 110;

            pos:= Find(MT_Comms.MT_Ser_Rx, '"', 2);
            pos2:= Find(MT_Comms.MT_Ser_Rx, '"', 3);
            MT_Meta.MT_Meta_MTSICS_Ver_0 = Mid(MT_Comms.MT_Ser_Rx, pos + 1, pos2 - pos - 1);
            MT_Diag.m_step = 111;

            pos:= Find(MT_Comms.MT_Ser_Rx, '"', 4);
            pos2:= Find(MT_Comms.MT_Ser_Rx, '"', 5);
            MT_Meta.MT_Meta_MTSICS_Ver_1 = Mid(MT_Comms.MT_Ser_Rx, pos + 1, pos2 - pos - 1);
            MT_Diag.m_step = 112;

            pos:= Find(MT_Comms.MT_Ser_Rx, '"', 6);
            pos2:= Find(MT_Comms.MT_Ser_Rx, '"', 7);
            MT_Meta.MT_Meta_MTSICS_Ver_2 = Mid(MT_Comms.MT_Ser_Rx, pos + 1, pos2 - pos - 1);
            MT_Diag.m_step = 113;

            pos:= Find(MT_Comms.MT_Ser_Rx, '"', 8);
            pos2:= Find(MT_Comms.MT_Ser_Rx, '"', 9);
            MT_Meta.MT_Meta_MTSICS_Ver_3 = Mid(MT_Comms.MT_Ser_Rx, pos + 1, pos2 - pos - 1);
            MT_Diag.m_step = 114;


            MT_Counters.MT_SICS_Resp_Count++;
            MT_Ctrl.MT_Ctrl_MT_SICS_Resp = 1;
            MT_Ops.MT_OK_Flag = 1;
        }
        //---MT-SICS level and version
        else if (MT_Diag._input_1_4 == "I1 I")
        {
            MT_Counters.MT_SICS_Resp_Count++;
            MT_Ctrl.MT_Ctrl_MT_SICS_Resp = -1;
            MT_Meta.MT_Meta_MTSICS_Level = "<Not avaliable>";
            MT_Meta.MT_Meta_MTSICS_Ver_0 = "<Not avaliable>";
            MT_Meta.MT_Meta_MTSICS_Ver_1 = "<Not avaliable>";
            MT_Meta.MT_Meta_MTSICS_Ver_2 = "<Not avaliable>";
            MT_Meta.MT_Meta_MTSICS_Ver_3 = "<Not avaliable>";
            MT_Ops.MT_OK_Flag = 0;
            Diagnostics.step = 120;
            MT_Diag.m_step = 8;
        }
        //--- balance data Format: I2 A <"Type> <Capacity> <Unit">
        //--- Ex: [I2 A "WMS404C-L WMS-Bridge 410.0090g"\r\n]
        else if (MT_Diag._input_1_4 == "I2 A")
        {
            MT_Counters.MT_Bal_Data_Resp_Count++;
            MT_Ctrl.MT_Ctrl_Balance_Data_Resp = 1;
            MT_Diag.m_step = 9;

            //---Find quote location
            pos:= Find(MT_Comms.MT_Ser_Rx, '"', 0);
            pos2:= Find(MT_Comms.MT_Ser_Rx, '"', 1);

            //---Get string between quotes
            temp:= Mid(MT_Comms.MT_Ser_Rx, pos + 1, pos2 - pos - 1);

            //---Find space locations
            pos3:= Find(temp, ' ', 0);
            pos4:= Find(temp, ' ', 0);

            MT_Meta.MT_Meta_Type = Mid(temp, pos3 + 1, pos4 - 1);
            MT_Meta.MT_Meta_Capacity = Mid(temp, pos4 + 1, pos2 - pos4 - 1);
            MT_Meta.MT_Meta_Unit = Mid(temp, 0, pos3);

            MT_Ops.MT_OK_Flag = 1;
            Diagnostics.step = 121;
            MT_Diag.m_step = 6;
        }
        //--- balance data
        else if (MT_Diag._input_1_4 == "I2 I")
        {
            MT_Ctrl.MT_Ctrl_Balance_Data_Resp = -1;
            MT_Meta.MT_Meta_Type = "<no data>";
            MT_Meta.MT_Meta_Capacity = "<no data>";
            MT_Meta.MT_Meta_Unit = "<no data>";
            MT_Ops.MT_OK_Flag = 0;
            Diagnostics.step = 7;
            MT_Diag.m_step = 122;
        }

        //---SW version and type def num: Format: [I3 A <"Software TDNR">]
        //---Example [I3 A "2.10 10.28.0.493.142"\r\n]
        else if (MT_Diag._input_1_4 == "I3 A")
        {

            pos:= Find(MT_Comms.MT_Ser_Rx, '"', 0);
            pos2:= Find(MT_Comms.MT_Ser_Rx, '"', 1);

            temp:= Mid(MT_Comms.MT_Ser_Rx, pos + 1, pos2 - pos - 1);
            pos3:= Find(temp, ' ', 0);

            MT_Meta.MT_Meta_SW_Ver = Mid(temp, 0, pos3 - 1);
            MT_Meta.MT_Meta_SW_Type = Mid(temp, pos3 + 1, Len(temp) - 1);

            MT_Counters.MT_SW_Ver_Resp_Count++;
            MT_Ctrl.MT_Ctrl_SW_Version_Resp = 1;
            MT_Ops.MT_OK_Flag = 1;
            Diagnostics.step = 130;
            MT_Diag.m_step = 141;
        }
        //---SW version and type def num
        else if (MT_Diag._input_1_4 == "I3 I")
        {
            MT_Ctrl.MT_Ctrl_SW_Version_Resp = -1;
            MT_Meta.MT_Meta_SW_Ver = "<Not avaliable>";
            MT_Meta.MT_Meta_SW_Type = "<Not avaliable>";
            MT_Ops.MT_OK_Flag = 0;
            Diagnostics.step = 131;
            MT_Diag.m_step = 151;
        }
        //---SN: Format: [I4 A <"SerialNumber">\r\n]
        //---Ex: [I4 A "B021002593"\r\n]
        else if (MT_Diag._input_1_4 == "I4 A")
        {
           pos:= Find(MT_Comms.MT_Ser_Rx, '"', 0);
            pos2:= Find(MT_Comms.MT_Ser_Rx, '"', 1);

            MT_Meta.MT_Meta_SN = Mid(MT_Comms.MT_Ser_Rx, pos + 1, pos2 - pos - 1);
            MT_Counters.MT_SN_Resp_Count++;
            MT_Ctrl.MT_Ctrl_SN_Resp = 1;
            MT_Ctrl.MT_Ctrl_Reset_Resp = 1;
            MT_Ops.MT_OK_Flag = 1;
            Diagnostics.step = 140;
            MT_Diag.m_step = 14;
        }
        //---SN
        else if (MT_Diag._input_1_4 == "I4 I")
        {
            MT_Ctrl.MT_Ctrl_SN_Resp = -1;
            MT_Ctrl.MT_Ctrl_Reset_Resp = -1;
            MT_Meta.MT_Meta_SN = "<Not avaliable>";
            MT_Ops.MT_OK_Flag = 0;
            Diagnostics.step = 141;
            MT_Diag.m_step = 15;
        }
        //---SW ID num: [I5 A <"SerialNumber">\r\n]
        //---Ex: [I5 A "12121306C"\r\n]
        else if (MT_Diag._input_1_4 == "I5 A")
        {
            pos:= Find(MT_Comms.MT_Ser_Rx, '"', 0);
            pos2:= Find(MT_Comms.MT_Ser_Rx, '"', 1);

            MT_Meta.MT_Meta_SW_ID = Mid(MT_Comms.MT_Ser_Rx, pos + 1, pos2 - pos - 1);
            MT_Ctrl.MT_Ctrl_SW_ID_Resp = 1;
            MT_Ops.MT_OK_Flag = 1;
            Diagnostics.step = 150;
            MT_Diag.m_step = 16;
        }
        //---SW ID num
        else if (MT_Diag._input_1_4 == "I5 I")
        {
            MT_Ctrl.MT_Ctrl_SW_ID_Resp = -1;
            MT_Meta.MT_Meta_SW_ID = "<Not avaliable>";
            MT_Ops.MT_OK_Flag = 0;
            Diagnostics.step = 151;
            MT_Diag.m_step = 17;
        }
        //---Unhandled Meta response
        else
        {
            MT_Ops.MT_OK_Flag = 1;
            Diagnostics.step = 152;
            MT_Diag.m_step = 18;
        }
    }

    else if (MT_Diag._input_1st_char == "M" || MT_Diag._input_2nd_char == "M")
    {
        Diagnostics.step = 200;
        MT_Diag.m_str = MT_Comms.MT_Ser_Rx;

        //  set weighing mode
        if (MT_Diag._input_1_3 == "M01")
        {
            //---response to query of weighing mode
            if (Mid(MT_Comms.MT_Ser_Rx, 4, 3) == " A ")
            {
                MT_Cfg.MT_Cfg_Weighing_Mode_Resp = TextToInt(Mid(MT_Comms.MT_Ser_Rx, 7, 2), 10);
                MT_Ops.MT_OK_Flag = 1;
                Diagnostics.step = 210;
                MT_Cfg.MT_Cfg_Wt_Mode_Ack = true;
            }
            //---Weighing mode is set
            else if (Mid(MT_Comms.MT_Ser_Rx, 4, 2) == " A")
            {
                MT_Ops.MT_OK_Flag = 1;
                Diagnostics.step = 211;
                MT_Cfg.MT_Cfg_Wt_Mode_Ack = true;
            }
            //---unable to set weighing mode
            else if (Mid(MT_Comms.MT_Ser_Rx, 4, 2) == " I")
            {
                MT_Ops.MT_OK_Flag = 0;
                Diagnostics.step = 212;
                MT_Cfg.MT_Cfg_Wt_Mode_Ack = false;
            }
            //---invalid parameter
            else if (Mid(MT_Comms.MT_Ser_Rx, 4, 2) == " L")
            {
                MT_Ops.MT_OK_Flag = 0;
                Diagnostics.step = 213;
                MT_Cfg.MT_Cfg_Wt_Mode_Ack = false;
            }
            else
            {
                MT_Ops.MT_OK_Flag = 0;
                Diagnostics.step = 214;
                MT_Cfg.MT_Cfg_Wt_Mode_Ack = false;
            }
        }

        //  set weighing env
        else if (MT_Diag._input_1_3 == "M02")
        {
            Diagnostics.step = 220;
            //---response to query of weighing env
            if (Mid(MT_Comms.MT_Ser_Rx, 4, 3) == " A ")
            {
                MT_Cfg.MT_Cfg_Weighing_Env_Resp = TextToInt(Mid(MT_Comms.MT_Ser_Rx, 7, 2), 10);
                MT_Ops.MT_OK_Flag = 1;
                Diagnostics.step = 221;
                MT_Cfg.MT_Cfg_Wt_Env_Ack = true;
            }
            //---Weighing env is set
            else if (Mid(MT_Comms.MT_Ser_Rx, 4, 2) == " A")
            {
                MT_Ops.MT_OK_Flag = 1;
                Diagnostics.step = 222;
            }
            //---unable to set weighing env
            else if (Mid(MT_Comms.MT_Ser_Rx, 4, 2) == " I")
            {
                MT_Ops.MT_OK_Flag = 0;
                Diagnostics.step = 223;
            }
            //---invalid parameter
            else if (Mid(MT_Comms.MT_Ser_Rx, 4, 2) == " L")
            {
                MT_Ops.MT_OK_Flag = 0;
                Diagnostics.step = 224;
            }
            else
            {
                MT_Ops.MT_OK_Flag = 0;
                Diagnostics.step = 225;
            }
        }

        //  set autozero mode
        else if (MT_Diag._input_1_3 == "M03")
        {
            Diagnostics.step = 230;
            //---response to query of autozero mode
            if (Mid(MT_Comms.MT_Ser_Rx, 4, 3) == " A ")
            {
                MT_Cfg.MT_Cfg_AutoZero_Resp = TextToInt(Mid(MT_Comms.MT_Ser_Rx, 7, 2), 10);
                MT_Ops.MT_OK_Flag = 1;
                Diagnostics.step = 231;
                MT_Cfg.MT_Cfg_AutoZero_Ack = true;
            }
            //---autozero mode is set
            else if (Mid(MT_Comms.MT_Ser_Rx, 4, 2) == " A")
            {
                MT_Ops.MT_OK_Flag = 1;
                Diagnostics.step = 232;
                MT_Cfg.MT_Cfg_AutoZero_Ack = true;
            }
            //---unable to set autozero mode
            else if (Mid(MT_Comms.MT_Ser_Rx, 4, 2) == " I")
            {
                MT_Ops.MT_OK_Flag = 0;
                Diagnostics.step = 233;
                MT_Cfg.MT_Cfg_AutoZero_Ack = false;
            }
            //---invalid parameter
            else if (Mid(MT_Comms.MT_Ser_Rx, 4, 2) == " L")
            {
                MT_Ops.MT_OK_Flag = 0;
                Diagnostics.step = 234;
                MT_Cfg.MT_Cfg_AutoZero_Ack = false;
            }
            else
            {
                MT_Ops.MT_OK_Flag = 0;
                Diagnostics.step = 235;
                MT_Cfg.MT_Cfg_AutoZero_Ack = false;
            }
        }


        //  set units
        //  Response: M21 A 1 1\r\n
        else if (MT_Diag._input_1_3 == "M21")
        {
            Diagnostics.step = 240;

            //---response to query
            if (Mid(MT_Comms.MT_Ser_Rx, 4, 3) == " A ")
            {
                MT_Diag.m_str = "<" + MT_Comms.MT_Ser_Rx + ">";
                MT_Cfg.MT_Cfg_Units_Resp = TextToInt(Mid(MT_Comms.MT_Ser_Rx, 9, 3), 10);
                MT_Ops.MT_OK_Flag = 1;
                Diagnostics.step = 241;
                MT_Cfg.MT_Cfg_Unit_Ack = true;
            }
            //---mode is set
            else if (Mid(MT_Comms.MT_Ser_Rx, 4, 2) == " A")
            {
                MT_Ops.MT_OK_Flag = 1;
                Diagnostics.step = 242;
                MT_Cfg.MT_Cfg_Unit_Ack = true;
            }
            //---unable to set mode
            else if (Mid(MT_Comms.MT_Ser_Rx, 4, 2) == " I")
            {
                MT_Ops.MT_OK_Flag = 0;
                Diagnostics.step = 243;
                MT_Cfg.MT_Cfg_Unit_Ack = false;
            }
            //---invalid parameter
            else if (Mid(MT_Comms.MT_Ser_Rx, 4, 2) == " L")
            {
                MT_Ops.MT_OK_Flag = 0;
                Diagnostics.step = 244;
                MT_Cfg.MT_Cfg_Unit_Ack = false;
            }
            else
            {
                MT_Ops.MT_OK_Flag = 0;
                Diagnostics.step = 245;
                MT_Cfg.MT_Cfg_Unit_Ack = false;
            }
        }
    }

    //---Handle Tare reponses
    else if (MT_Diag._input_1_2 == "T ")
    {
        MT_Counters.MT_Tare_Resp_Count++;
        Diagnostics.step = 300;
        if (MT_Diag._input_1_3 == "T I")
        {
            MT_Ops.MT_OK_Flag = 0;
            MT_Ctrl.MT_Ctrl_Tare_Resp = -1;
            Diagnostics.step = 301;
        }
        else if (MT_Diag._input_1_3 == "T S")
        {
            MT_Ops.MT_OK_Flag = 1;
            MT_Ctrl.MT_Ctrl_Tare_Resp = 1;
            Diagnostics.step = 302;
        }
        else if (MT_Diag._input_1_3 == "T +")
        {
            MT_Ops.MT_OK_Flag = 0;
            MT_Ctrl.MT_Ctrl_Tare_Resp = -1;
            Diagnostics.step = 303;
        }
        else if (MT_Diag._input_1_3 == "T -")
        {
            MT_Ops.MT_OK_Flag = 0;
            MT_Ctrl.MT_Ctrl_Tare_Resp = -1;
            Diagnostics.step = 304;
        }
        else
        {
            MT_Ops.MT_OK_Flag = 0;
            MT_Ctrl.MT_Ctrl_Tare_Resp = -1;
            Diagnostics.step = 305;
        }

    }

    else if (MT_Diag._input_1_3 == "TA ")
    {
        Diagnostics.step = 310;
    }
    else if (MT_Diag._input_1_4 == "TAC ")
    {
        Diagnostics.step = 320;
    }

    //---Handle Tare immediately respones
    else if (MT_Diag._input_1_3 == "TI ")
    {
        Diagnostics.step = 330;
    }

    //---Handle weight measurement broadcasts
    else if (MT_Diag._input_1st_char == "S" || MT_Diag._input_2nd_char == "S")
    {
        Diagnostics.step = 400;
        //---Good weight data, not stable
        if (MT_Diag._input_1_3 == "S D")
        {
            MT_Ops.MT_Base_Weight_PV = TextToFloat(Mid(MT_Comms.MT_Ser_Rx, 5, MT_Ops.MT_Base_Weight_Input_Len - 3));
            MT_Ops.MT_Data_Stable = 0;
			MT_Counters._stale_count--;
			MT_Counters._PV_Sample_Cnt++;
            Diagnostics.step = 401;
			MT_Ctrl.MT_Ctrl_Zero_Resp = 0;
        }


        //---Scale busy can't return PV
        else if (MT_Diag._input_1_3 == "S I")
        {
            MT_Counters._stale_count++;
            Diagnostics.step = 402;
        }

        //---Good weight data, stable
        else if (MT_Diag._input_1_3 == "S S")
        {
            MT_Ops.MT_Base_Weight_PV = TextToFloat(Mid(MT_Comms.MT_Ser_Rx, 5, MT_Ops.MT_Base_Weight_Input_Len - 3));
            MT_Ops.MT_Data_Stable = 1;
            MT_Counters._stale_count--;
			MT_Counters._PV_Sample_Cnt++;
            Diagnostics.step = 403;
			MT_Ctrl.MT_Ctrl_Zero_Resp = 0;
        }

        //---Weight exceeds max: can't return PV
        else if (MT_Diag._input_1_3 == "S +")
        {
            MT_Ops.MT_Data_Stable = 0;
            MT_Counters._stale_count++;
            Diagnostics.step = 404;
        }

        //---Weight less than min: can't return PV
        else if (MT_Diag._input_1_3 == "S -")
        {
            MT_Ops.MT_Data_Stable = 0;
            MT_Counters._stale_count++;
            Diagnostics.step = 405;
        }
    }
    //--- Handle zero responses
    else if (MT_Diag._input_1_2 == "Z ")
    {
        Diagnostics.step = 12;
        MT_Counters.MT_Zero_Resp_Count++;
        Diagnostics.step = 500;
        // Zero scale OK
        if (MT_Diag._input_1_3 == "Z A")
        {
            MT_Ctrl.MT_Ctrl_Zero_Resp = 1;
            MT_Ops.MT_OK_Flag = 1;
            Diagnostics.step = 501;
			MT_Counters._stale_count--;
        }
        // Zero scale failed: scale busy
        else if (MT_Diag._input_1_3 == "Z I")
        {
            MT_Ctrl.MT_Ctrl_Zero_Resp = -1;
            MT_Ops.MT_Data_Stable = 0;
            MT_Ops.MT_OK_Flag = 0;
            Diagnostics.step = 502;
        }
        // Zero scale failed: above max
        else if (MT_Diag._input_1_3 == "Z +")
        {
            MT_Ctrl.MT_Ctrl_Zero_Resp = -1;
            MT_Ops.MT_Data_Stable = 0;
            MT_Ops.MT_OK_Flag = 0;
            Diagnostics.step = 503;
        }
        // Zero scale failed: below min
        else if (MT_Diag._input_1_3 == "Z -")
        {
            MT_Ctrl.MT_Ctrl_Zero_Resp = -1;
            MT_Ops.MT_Data_Stable = 0;
            MT_Ops.MT_OK_Flag = 0;
            Diagnostics.step = 504;
        }
    } //--- Z

    //--- Handle zero immediately responses
    else if (MT_Diag._input_1_2 == "ZI")
    {
        Diagnostics.step = 510;
        // Zero scale Immediately: weight is not stable
        if (MT_Diag._input_1_4 == "ZI D")
        {
			MT_Counters._stale_count--;
            MT_Ops.MT_OK_Flag = 1;
            MT_Ops.MT_Data_Stable = 0;
            Diagnostics.step = 511;
        }
        // Zero scale Immediately: weight is stable
        else if (MT_Diag._input_1_4 == "ZI S")
        {
			MT_Counters._stale_count--;
            MT_Ops.MT_OK_Flag = 1;
            MT_Ops.MT_Data_Stable = 1;
            Diagnostics.step = 512;
        }
        // Zero scale Immediately failed: scale busy
        else if (MT_Diag._input_1_4 == "ZI I")
        {
            MT_Ops.MT_OK_Flag = 0;
            MT_Ops.MT_Data_Stable = 0;
            Diagnostics.step = 513;
        }
        // Zero scale Immediately failed: above max
        else if (MT_Diag._input_1_4 == "ZI +")
        {
            MT_Ops.MT_OK_Flag = 0;
            MT_Ops.MT_Data_Stable = 0;
            Diagnostics.step = 514;
        }
        // Zero scale Immediately failed: below min
        else if (MT_Diag._input_1_4 == "ZI -")
        {
            MT_Ops.MT_OK_Flag = 0;
            MT_Ops.MT_Data_Stable = 0;
            Diagnostics.step = 515;
        }
        // Zero scale Immediately failed: below min
        else
        {
            MT_Ops.MT_OK_Flag = 0;
            MT_Ops.MT_Data_Stable = 0;
            Diagnostics.step = 516;
        }
    } //---End ZI
}




if (MT_Counters._stale_count < 0)
    MT_Counters._stale_count = 0;

if (MT_Counters._stale_count > GatewaySettings.MT_Stale_Cnt_Max)
    MT_Counters._stale_count = GatewaySettings.MT_Stale_Cnt_Max;



if (MT_Counters._stale_count > GatewaySettings.MT_Stale_Data_Threshold)
    MT_Ops.MT_OK_Flag = 0;
else
    MT_Ops.MT_OK_Flag = 1;


if(Gateway_Diag.MTS_Override) {
    MT_Ops.MT_OK_Flag = 1;
    MT_Ops.MT_Data_Stable = 1;
    MT_Ops.MT_Base_Weight_PV = 123.45;
}

}
