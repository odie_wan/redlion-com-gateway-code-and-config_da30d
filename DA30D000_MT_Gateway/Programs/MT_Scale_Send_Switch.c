void MT_Scale_Send_Switch(void)
{
// MT_Ctrl_Req_Idx - psuedo enumeration
// -- 0 - no request
// -- 1 - MT-SICS
// -- 2 - Balance Data
// -- 3 - SW Ver
// -- 4 - SN
// -- 5 - SW ID

// MT_Cfg_Req_Idx - psuedo enumeration
// -- 0 - No request
// -- 1 - Autozero
// -- 2 - Wt Env
// -- 3 - Wt Mode
// -- 4 - Units chan 0
// -- 5 - Units chan 1

MT_Comms.MT_Ser_Tx = "<empty>";

if (MT_Cfg.MT_Cfg_Units_Cmd > 20)
    MT_Cfg.MT_Cfg_Units_Cmd = 0;

if (MT_Cfg.MT_Cfg_Units_Cmd < 0)
    MT_Cfg.MT_Cfg_Units_Cmd = 20;

if(MT_Cfg.MT_Cfg_Set_Update_Req == 1)
	MT_Comms.MT_Ser_Tx = "UPD "  + IntToText(MT_Cfg._Update_Rate_Resp, 10, 2) + "\r\n";	

if(MT_Cfg.MT_Cfg_Def_OpMode_Req == 1)
	MT_Comms.MT_Ser_Tx = "I26\r\n";

if(MT_Cfg.MT_Cfg_Set_User_Mode_Req == 1)
	MT_Comms.MT_Ser_Tx = "M31 0\r\n";				       

//---Request scale to send PV data constantly
if(MT_Ctrl._Cont_Data_Req == 1)
	MT_Comms.MT_Ser_Tx = "SIR\r\n";

                                             
//---Add send pv constantly req to startup cmd list
if(MT_Ctrl._Cont_Data_Req_rs == 1)
	MT_Comms.MT_Ser_Tx = "M44 0 SR 1 g\r\n";


if(MT_Ctrl.MT_Ctrl_Reset_Req == 1)
    MT_Comms.MT_Ser_Tx = "@\r\n";


if(MT_Ctrl.MT_Ctrl_Zero_Req == 1)
    MT_Comms.MT_Ser_Tx = "Z\r\n";

if(MT_Ctrl.MT_Ctrl_Tare_Req == 1)
    MT_Comms.MT_Ser_Tx = "T\r\n";

if (MT_Ctrl.MT_Ctrl_All_Meta_Req_Shadow != MT_Ctrl.MT_Ctrl_All_Meta_Req && MT_Ctrl.MT_Ctrl_All_Meta_Req)
    MT_Ctrl.MT_Ctrl_Req_Idx = 1;

switch (MT_Ctrl.MT_Ctrl_Req_Idx)
{
    case 1://-- MT-SICS
        MT_Comms.MT_Ser_Tx = "I1\r\n";
        break;

    case 2: //--Balance Data
        MT_Comms.MT_Ser_Tx = "I2\r\n";
        break;

    case 3: //--SW Ver
        MT_Comms.MT_Ser_Tx = "I3\r\n";
        break;

    case 4: //--SN
        MT_Comms.MT_Ser_Tx = "I4\r\n";
        break;

    case 5: //--SW ID
        MT_Comms.MT_Ser_Tx = "I5\r\n";
        break;

    default:
        break;
}

if (MT_Ctrl.MT_Ctrl_Req_Idx > 0)
    MT_Ctrl.MT_Ctrl_Req_Idx++;

if (MT_Ctrl.MT_Ctrl_Req_Idx > 5)
    MT_Ctrl.MT_Ctrl_Req_Idx = 0;


if (MT_Cfg.MT_Cfg_Req_All_Req_Shadow != MT_Cfg.MT_Cfg_Req_All_Req && MT_Cfg.MT_Cfg_Req_All_Req)
    MT_Cfg.MT_Cfg_Req_Idx = 1;


switch (MT_Cfg.MT_Cfg_Req_Idx)
{
    case 1: //--Query Cfg Autozero
        MT_Comms.MT_Ser_Tx = "M03\r\n";
        break;

    case 2: //--Query Cfg Wt Env
        MT_Comms.MT_Ser_Tx = "M02\r\n";
        break;

    case 3: //--Query Cfg Wt Mode
        MT_Comms.MT_Ser_Tx = "M01\r\n";
        break;

    case 4: //--Query Cfg Units
        MT_Comms.MT_Ser_Tx = "M21\r\n";
        break;

    default:
        break;
}


if (MT_Cfg.MT_Cfg_Req_Idx > 0)
    MT_Cfg.MT_Cfg_Req_Idx++;

if (MT_Cfg.MT_Cfg_Req_Idx > 4)
    MT_Cfg.MT_Cfg_Req_Idx = 0;



if (MT_Cfg.MT_Cfg_Set_All_Req_Shadow != MT_Cfg.MT_Cfg_Set_All_Req && MT_Cfg.MT_Cfg_Set_All_Req)
    MT_Cfg.MT_Cfg_Set_Idx = 1;


switch (MT_Cfg.MT_Cfg_Set_Idx)
{
    case 1: //--Set Cfg Autozero
        MT_Comms.MT_Ser_Tx = "M03\r\n";
        break;

    case 2: //--Set Cfg Wt Env
        MT_Comms.MT_Ser_Tx = "M02\r\n";
        break;

    case 3: //--Set Cfg Wt Mode
        MT_Comms.MT_Ser_Tx = "M01\r\n";
        break;

    case 4:  //--Set Cfg Units
        MT_Comms.MT_Ser_Tx = "M21 0 " + IntToText(MT_Cfg.MT_Cfg_Units_Cmd, 10, 2) + "\r\n";
        break;

    case 5:  //--Set Cfg Units
        MT_Comms.MT_Ser_Tx = "M21 1 " + IntToText(MT_Cfg.MT_Cfg_Units_Cmd, 10, 2) + "\r\n";
        break;

    default:
        break;
}

if (MT_Cfg.MT_Cfg_Set_Idx > 0)
    MT_Cfg.MT_Cfg_Set_Idx++;

if (MT_Cfg.MT_Cfg_Set_Idx > 5)
    MT_Cfg.MT_Cfg_Set_Idx = 0;




MT_Cfg.MT_Cfg_Set_All_Req_Shadow = MT_Cfg.MT_Cfg_Set_All_Req;
MT_Cfg.MT_Cfg_Req_All_Req_Shadow = MT_Cfg.MT_Cfg_Req_All_Req;
MT_Ctrl.MT_Ctrl_All_Meta_Req_Shadow = MT_Ctrl.MT_Ctrl_All_Meta_Req;

if (MT_Comms.MT_Ser_Tx != "<empty>")  {
    PortPrint(MT_Comms.MT_Ser_Port, MT_Comms.MT_Ser_Tx);  
    MT_Comms.MT_Ser_Tx_Bytes += Len(MT_Comms.MT_Ser_Tx);
    Push_MT_Tx_Buff(MT_Comms.MT_Ser_Tx);
                              
}
}
