void RS232_Exec(void)
{
Counters.rs232_exe_cnt++;
	
    switch(MT_Diag.MT_Scale_Eng_State) {
        // Defines.MT_SCALE_ENG_BOOT
        case 0:
			//---Autostart after 300 cycles
            if(Counters.rs232_exe_cnt == 300) {
                MT_Diag.MT_Scale_Eng_State = Defines.MTS_ENG_START;
				
            }
            Counters.rs232_boot_cnt++;
            break;


        // Defines.MT_SCALE_ENG_IDLE
		//---Handle manual start using start/stop tags---
        case 1:
			if(MT_Eng.Start == 1 && MT_Eng.Start != MT_Eng.StartShadow) {
	            MT_Diag.MT_Scale_Eng_State++;
				MT_Counters._start_sync_count = 0;

    		}                                    
			
			
            Counters.rs232_idle_cnt++;                                    

            break;


        //  Defines.MT_SCALE_ENG_START                   
        case 2:        
                                             
			switch(MT_Counters._start_sync_count) {
				case 1:
					MT_Ctrl.MT_Ctrl_Reset_Req = true;
					break;

				case 5:
					MT_Ctrl.MT_Ctrl_Reset_Req = false;
					break;

				case 10:
					MT_Ctrl._Cont_Data_Req = true;
					break;

				case 15:
					MT_Ctrl._Cont_Data_Req = false;
					break;     

				case 20:
					MT_Diag.MT_Scale_Eng_State = Defines.MTS_ENG_OPERATIONAL;
					break;
			}

			MT_Scale_Send_Switch();
            MT_Scale_Master_Parse();
              if(MT_Eng.Stop == 1 && MT_Eng.StopShadow != MT_Eng.Stop) {
				MT_Diag.MT_Scale_Eng_State = Defines.MTS_ENG_IDLE;
				Counters.rs232_start_cnt = 0;
            }
			MT_Counters._start_sync_count++;
            Counters.rs232_start_cnt++;
            break;

        // Defines.MT_SCALE_ENG_OPERATIONAL
		//---Main operational mode: send requests to scale, parse replies from scale
        case 3:
            Counters.rs232_op_cnt++;


			MT_Scale_Send_Switch();
            MT_Scale_Master_Parse();

            if(MT_Eng.Stop == 1 && MT_Eng.StopShadow != MT_Eng.Stop) {
				MT_Diag.MT_Scale_Eng_State = Defines.MTS_ENG_IDLE;
				Counters.rs232_start_cnt = 0;
            }

			if(MT_Ctrl.Reset_Engine_Req == 1 && MT_Ctrl.Reset_Engine_Req != MT_Ctrl.Reset_Engine_Req_Shadow){
				MT_Diag.MT_Scale_Eng_State = Defines.MTS_ENG_RESET;
                Counters.rs232_start_cnt = 0;                                                                                                
			}

            break;

        // Defines.MT_SCALE_ENG_RESET                                               
		//---Manualy commanded restart
        case 4:
            Counters.rs232_reset_cnt++;
            reset_MTS_data();

            MT_Diag.MT_Scale_Eng_State = Defines.MTS_ENG_START;

            break;
        

        //  Defines.MT_SCALE_ENG_MAINT
		//---TODO: do we even need this
        case 5: 
            Counters.rs232_maint_cnt++;
            break;


        default:
            Counters.rs232_default_cnt++;
            break;

    }

MT_Eng.StartShadow = MT_Eng.Start;
MT_Eng.StopShadow = MT_Eng.Stop;
MT_Ctrl.Reset_Engine_Req_Shadow = MT_Ctrl.Reset_Engine_Req;
Sleep(GatewaySettings.MTS_Polling_Delay);

}
