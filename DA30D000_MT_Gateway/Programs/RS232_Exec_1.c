void RS232_Exec_1(void)
{
Counters.rs232_exe_count++;


    switch(MT_Diag.MT_Scale_Eng_State) {
        // Defines.MT_SCALE_ENG_BOOT
        case 0:
			//---Autostart after 300 cycles
            if(Counters.rs232_exe_count == 300) {
                MT_Diag.MT_Scale_Eng_State = Defines.MTS_ENG_START;
				MT_Ctrl._Cont_Data_Req = true;
            }
            Counters.rs232_boot_count++;
            break;


        // Defines.MT_SCALE_ENG_IDLE
		//---Handle manual start using start/stop tags---
        case 1:
			if(MT_Eng.Start == 1 && MT_Eng.Start != MT_Eng.StartShadow) {
	            MT_Diag.MT_Scale_Eng_State++;
    		}                                    

            Counters.rs232_idle_count++;                                    

            break;


        //  Defines.MT_SCALE_ENG_START                   
		//---One cycle here to reset everything
        case 2:
            MT_Diag.MT_Scale_Eng_State = Defines.MTS_ENG_OPERATIONAL;
            reset_MTS_data();
            Counters.rs232_start_count++;
            break;

        // Defines.MT_SCALE_ENG_OPERATIONAL
		//---Main operational mode: send requests to scale, parse replies from scale
        case 3:
            Counters.rs232_op_count++;
	
			MT_Scale_Send_Switch();
            MT_Scale_Master_Parse();

			MT_Ctrl._Cont_Data_Req = false;
            if(MT_Eng.Stop == 1 && MT_Eng.Stop != MT_Eng.Stop) {
                Counters.rs232_reset_count++;
                reset_MTS_data();
				MT_Diag.MT_Scale_Eng_State = Defines.MTS_ENG_IDLE;
            }


            break;

        // Defines.MT_SCALE_ENG_RESET                                               
		//---Manualy commanded restart
        case 4:
            Counters.rs232_reset_count++;
            reset_MTS_data();

            break;
        

        //  Defines.MT_SCALE_ENG_MAINT
		//---TODO: do we even need this
        case 5: 
            Counters.rs232_main_count++;
            break;


        default:
            Counters.rs232_default_count++;
            break;

    }

MT_Eng.StartShadow = MT_Eng.Start;
MT_Eng.StopShadow = MT_Eng.Stop;
Sleep(GatewaySettings.MTS_Polling_Delay);

}
