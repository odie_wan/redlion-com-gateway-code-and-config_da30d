void RS485_Exec(void)
{
int i = 0;
	
    Counters.rs485_exe_cnt++;


    switch (GF40s.GF40_Engine_State)
    {
        case 0: //---boot
            if(Counters.rs485_exe_cnt == 300) {
                GF40s.GF40_Engine_State = Defines.GF40_ENG_START;
            }

            Counters.gf40_exe_boot_cnt++;
            break;

        case 1: //---Idle
            if(GF40s.GF40_Start == true && GF40s.GF40_Start != GF40s.GF40_StartShdw){
		        GF40s.GF40_Engine_State++;
            }

            GF40_Rx();
            Counters.gf40_exe_idle_cnt++;
            break;


        case 2: //---start---
            GF40s.GF40_Engine_State = Defines.GF40_ENG_OPERATIONAL;
            reset_gf40_data();
            Counters.gf40_exe_start_cnt++;
            break;



        case 3: //---operational---
            Counters.gf40_exe_op_cnt++;
            MFC_Exec_State_Engine();
            MFC_CfgRead_State_Engine();
            MFC_Err_State_Engine();
            MFC_Op_State_Engine();
            if(GF40s.GF40_Stop == false && GF40s.GF40_Stop != GF40s.GF40_StopShdw) {
                GF40s.GF40_Engine_State = Defines.GF40_ENG_IDLE;            
                Counters.gf40_exe_reset_cnt++;
                reset_gf40_data();
            }            									       
     
            break;

        case 4: //---Reset
            Counters.gf40_exe_reset_cnt++;
            reset_gf40_data();
            break;

        
        default:
            Counters.gf40_exe_maint_default_cnt++;
            break;
    }


    GF40s.GF40_StartShdw = GF40s.GF40_Start;
    GF40s.GF40_StopShdw = GF40s.GF40_Stop;
    Sleep(GatewaySettings.MFC_Polling_Delay);
}
