void reset_gf40_data_1(void)
{

    int i = 0;
    for(i = 0; i < GatewaySettings.NUM_MFCs; i++) {
        Counters.gf40_exe_reset_cnt++;
        GF40s.GF40_Idx = 0;
        GF40s0.Id[i] = "0";
        GF40s.GF40_ExecState = 0;

        GF40s0.SerRxBytes[i] = 0;
        GF40s0.SerTxBytes[i] = 0;
        GF40s0.MFC_ACK[i] = 0;

        GF40s0.CfgWriteQueryCount[i] = 0;
        GF40s0.CfgReadQueryCount[i] = 0;
        GF40s0.OpQueryCount[i] = 0;
        GF40s0.ErrQueryCount[i] = 0;

        GF40s0.CfgReadQueryState[i] = 0;
        GF40s0.OpQueryState[i] = 0;
        GF40s0.CfgWriteQueryState[i] = 0;
        GF40s0.ErrQueryState[i] = 0;



        GF40s0.OpWaitCnt[i] = 0;
        GF40s0.CfgRWaitCnt[i] = 0;
        GF40s0.ErrWaitCnt[i] = 0;


        GF40s0.ErrWaitCnt[i] = 0;
        GF40s0.ErrWaitCnt[i] = 0;
        GF40s0.ErrRespCount[i] = 0;
        GF40s0.CfgReadRespCount[i] = 0;
        GF40s0.OpRespCnt[i] = 0;

    }
}
