void reset_MTS_data(void)
{

    int i = 0;
    Counters.rs232_exe_cnt++;
    MT_Comms.MT_Ser_Tx                  = "";
    MT_Comms.MT_Ser_Rx                  = "";
    MT_Comms.MT_Ser_Rx_Bytes            = 0;
    MT_Comms.MT_Ser_Tx_Bytes            = 0;

    MT_Counters._parse_count            = 0;
    MT_Counters._stale_count            = 0;
	MT_Counters._start_sync_count 		= 0;
    MT_Counters.MT_Bal_Data_Resp_Count  = 0;	
    MT_Counters.MT_Reset_Resp_Count	    = 0;	
    MT_Counters.MT_SICS_Resp_Count	    = 0;	
    MT_Counters.MT_SN_Resp_Count	    = 0;	
    MT_Counters.MT_SW_ID_Resp_Count	    = 0;	
    MT_Counters.MT_SW_Ver_Resp_Count    = 0;	
    MT_Counters.MT_Tare_Resp_Count	    = 0;	
    MT_Counters.MT_Zero_Resp_Count	    = 0;	
    MT_Counters._stale_count            = 0;

    MT_Meta.MT_Meta_MTSICS_Level        = "";
    MT_Meta.MT_Meta_Capacity	        = "";
    MT_Meta.MT_Meta_MTSICS_Ver_0        = "";
    MT_Meta.MT_Meta_MTSICS_Ver_1        = "";
    MT_Meta.MT_Meta_MTSICS_Ver_2        = "";
    MT_Meta.MT_Meta_MTSICS_Ver_3        = "";
    MT_Meta.MT_Meta_SN                  = "";
    MT_Meta.MT_Meta_SW_ID		        = "";
    MT_Meta.MT_Meta_SW_Type		        = "";
    MT_Meta.MT_Meta_SW_Ver		        = "";
    MT_Meta.MT_Meta_Type		        = "";
    MT_Meta.MT_Meta_Unit		        = "";

    MT_Ops.MT_Data_Stable	            = 0;
    MT_Ops.MT_OK_Flag	                = 0;
    MT_Ops.MT_Base_Weight_Input_Len     = 0;
    MT_Ops.MT_Base_Weight_PV	        = 0;
	MT_Ctrl.MT_Ctrl_Reset_Resp 			= 0;
	MT_Ctrl.MT_Ctrl_Zero_Resp			= 0;
	MT_Ctrl.MT_Ctrl_Tare_Resp			= 0;


}
