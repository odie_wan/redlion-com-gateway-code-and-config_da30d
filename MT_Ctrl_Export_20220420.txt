
[Flag.5.2]

Name	Value	Extent	Persist	Sim	OnWrite	HasSP	Label	Alias	Desc	Class	FormType	Format / On	Format / Off	ColType	Color / On	Color / Off	Event1 / Mode	Event2 / Mode	Trigger1 / Mode	Trigger2 / Mode	Sec / Access	Sec / Logging
MT_Ctrl.MT_Ctrl_All_Meta_Req		0	Non-Retentive			No	All Meta rq				Two-State			Two-State	Lime on Black	Red on Black	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_Cont_Data_Req		0	Non-Retentive			No					Two-State			Two-State	Lime on Black	Red on Black	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object

[Numeric.0.0]

Name	Value	Extent	TreatAs	Persist	ScaleTo	Sim	OnWrite	HasSP	Label	Alias	Desc	Class	FormType	LimitMin	LimitMax	LimitType	Deadband	ColType	Event1 / Mode	Event2 / Mode	Trigger1 / Mode	Trigger2 / Mode	QuickPlot / Mode	Sec / Access	Sec / Logging
MT_Ctrl.MT_Ctrl_All_Meta_Req_Shadow		0	Signed Integer	Non-Retentive	Do Not Scale			No	All Meta Req Shadow				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_Balance_Data_Req		0	Signed Integer	Non-Retentive	Do Not Scale			No	Bal Data rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_Balance_Data_Resp		0	Signed Integer	Non-Retentive	Do Not Scale			No	Bal Data rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_Config_Req		0	Signed Integer	Non-Retentive	Do Not Scale			No	Config rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_Config_Resp		0	Signed Integer	Non-Retentive	Do Not Scale			No	Config rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_MT_SICS_Req		0	Signed Integer	Non-Retentive	Do Not Scale			No	MT-SICS rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_MT_SICS_Resp		0	Signed Integer	Non-Retentive	Do Not Scale			No	MT-SICS rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_Req_Idx		0	Signed Integer	Non-Retentive	Do Not Scale			No	Req Idx				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_Reset_Req		0	Signed Integer	Non-Retentive	Do Not Scale			No	Reset rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_Reset_Resp		0	Signed Integer	Non-Retentive	Do Not Scale			No	Reset rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_SN_Req		0	Signed Integer	Non-Retentive	Do Not Scale			No	SN rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_SN_Resp		0	Signed Integer	Non-Retentive	Do Not Scale			No	SN rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_SW_ID_Req		0	Signed Integer	Non-Retentive	Do Not Scale			No	SW ID rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_SW_ID_Resp		0	Signed Integer	Non-Retentive	Do Not Scale			No	SW ID rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_SW_Version_Req		0	Signed Integer	Non-Retentive	Do Not Scale			No	SW Ver rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_SW_Version_Resp		0	Signed Integer	Non-Retentive	Do Not Scale			No	SW Ver rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_Tare_Req		0	Signed Integer	Non-Retentive	Do Not Scale			No	Tare rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_Tare_Resp		0	Signed Integer	Non-Retentive	Do Not Scale			No	Tare rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_Zero_Req		0	Signed Integer	Non-Retentive	Do Not Scale			No	Zero rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
MT_Ctrl.MT_Ctrl_Zero_Resp		0	Signed Integer	Non-Retentive	Do Not Scale			No	Zero rq				General			Automatic		General	Disabled	Disabled	Disabled	Disabled	Disabled	Default for Object	Default for Object
